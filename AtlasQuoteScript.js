let jsdom = require('jsdom'),
    fs = require('fs'),
    request = require('request');

let htmlFileName = 'PoeMap.html';

// Save HTML file locally
request("http://www.pathofexile.gamepedia.com/Map", function (error, response, body) {
    if (!error) {
        saveHTML(body);
        saveFlavours();
    } else {
        console.log(error);
    }
});

function saveHTML(body){
  console.log("Saving HTML...");
  let file = fs.createWriteStream(htmlFileName);
  file.on('error', function(err) {console.error("Error saving HTML: " + err);});
  file.write(body);
  file.end();
  console.log("Saved html file")
}

// Save flavour texts to a text file
function saveFlavours(){
  let result = {};
  jsdom.env(
    htmlFileName,
    ["http://code.jquery.com/jquery.js"],
    function (err, window) {
      let $ = window.$;
      
      $('span.inline-infobox-container').each(function(){
        let mapName = $(this).find('span.header.-single').text();
        let flavourText = $(this).find('span.group.-textwrap.tc.-flavour').text();
        
        if(flavourText && mapName)
          result[mapName] = flavourText;
      });

      console.log("Saving flavours...");
      let fileName = 'PoEMapFlavours.txt';
      let file = fs.createWriteStream(fileName);
      file.on('error', function(err) {console.error("Error saving flavours: " + err);});
      Object.keys(result).forEach(function(key){
        file.write(" - " + key + " - " + "\n" + result[key] + "\n\n")
      });
      file.end();
      console.info("Flavours saved to " + fileName);
    }
  );
}
