# Path of Exile - Map Flavour Text Parser #
### Requirements ###

* NPM
* NodeJS 

### Installation ###

1. Run ```npm install``` where the package.json is located to install required dependencies.

2. Run the script with ```node AtlasQuoteScript.js``` and it saves the flavours to a text file on the same folder where the script is located.

### UPDATES ###

This doesn't work anymore since Path of Exile was updated to 3.0 and a lot of things change to maps